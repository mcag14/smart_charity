<?php

namespace App\Http\Controllers;
use Session;
use App\guest;
use App\guestpay;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getdata(Request $request)
    {

    }
    public function guestpay()
    {
        //query to Database
       // $registers = DB::table('users')
       //              ->whereNull('status')
       //              ->get();
       // $registers = User::where('status','3')->get();

       $guestpays = guestpay::where('status','1')->get();
        return view('chiefcordinator.guestpays' , ['guestpays'=>$guestpays]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function showguestpayment(Request $request)
   {
     $registers = User::where('status','3')->get();
      return view('chiefcordinator.accound_approvel' , ['registers'=>$registers]);
   }
    public function guestupdate(Request $request)
    {
      $request->validate([
        'username'   =>  'required',
        'mail'   =>  'required',
      ]);
      $guest = new guest;
      $guest->guest_id= "Guest".(1000+$guest->id);
      $guest->name    = $request->username;
      $guest->email    = $request->mail;
      $guest->type    = "guest_member";
      $guest->save();
      $guest->guest_id= "Guest1".($guest->id);
      $guest->save();
      return view('guestpayment');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function show(guest $guest)
    {
        //
    }
    public function guestpaym( Request $request)
    {
      $mytime = Carbon::now();
      $request->validate([
        'Amount'        =>  'required|integer',
        'paymentnumber' =>  'required|integer',
      ]);

      $guestpay = new guestpay;
      $guestpay->amount          =  $request->Amount;
      $guestpay->method          =   $request->method;
      $guestpay->method_number   = $request->paymentnumber;
      $guestpay->date           = $mytime;
      $guestpay->status         = 1;
      $guestpay->save();

      // $guestpay->guest_id        = "guest".(1000+$guestpay->id);

      Session::flash("msg","your payment was successfully compleated");
      return view('guestpayment');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function edit(guest $guest)
    {
      // $mytime = Carbon::now()->toDateString();
      // return Carbon::now()->month;
      //  if ($request->guestpayin = 'today') {
      //    $data = guestpay::where('date',$mytime)->get();
      //  }
      //  elseif ($request->guestpayin = 'week') {
      //    $date = \Carbon\Carbon::today()->subDays(7);
      //    $data = guestpay::where('date','<=',$date)->get();
      //  }
      //  elseif ($request->guestpayin = 'month') {
      //    $data = guestpay::where()->get();
      //  }
      //  elseif ($request->guestpayin = 'year') {
      //    $data = guestpay::->whereYear('date','<=',Carbon::now()->year)->get();
      //  }
      //  return view('chiefcordinator.guestpays')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function payupdate($id)
    {
      $guestpay=guestpay::find($id);
      $guestpay->status=2;
      $guestpay->save();
      // Session::flash("msg","Registration Approved!");
      return back()->with('success','Registration Approved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function paydestroy($id)
    {
      guestpay::where('id','=',$id)->delete();
      return back();
    }
}

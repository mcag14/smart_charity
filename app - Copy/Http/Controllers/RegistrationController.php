<?php

namespace App\Http\Controllers;
use Illuminate\support\Facades\Hash;
use Session;
use App\user;
use Auth;
use Illuminate\Http\Request;
// use App\Http\Controllers\Auth;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view("form");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
        'fname'   =>  'required|string|max:255',
        'lname'   =>  'required|string|max:255',
        'house'   =>  'required|string|max:255',
        'streets' =>  'required|string|max:255',
        'landmark'=>  'required|string|max:255',
        'state'   =>  'required',
        'district'=>  'required',
        'pin'     =>  'required|string|max:6|min:6',
        'gender'  =>  'required',
        'zone'    =>  'required',
        'unit'    =>  'required',
        'phone'   =>  'required|string|max:10|min:10',
        'email'   =>  'required|string|email|max:255|unique:users',
        'passwd'  =>  'required|string|min:6',

      ]);
      $user = new user;
      $user->fname    = $request->fname;
      $user->lname    = $request->lname;
      $user->house    = $request->house;
      $user->street   = $request->streets;
      $user->landmark = $request->landmark;
      $user->state    = $request->state;
      $user->district = $request->district;
      $user->pincode  = $request->pin;
      $user->gender   = $request->gender;
      $user->zone     = $request->zone;
      $user->unit     = $request->unit;
      $user->number   = $request->phone;
      $user->email    = $request->email;
      $user->password = bcrypt($request->passwd);
      $user->type     = "member";
      $user->status   = 1;
      $user->save();
      Session::flash("msg","your registration successfully compleated please waite for approvel");

      // $user->save();
      // $id=$user->id;
      // $user->userid = $id;
      // $user->save();
      return back();
    }
    public function unit(Request $request)
    {
      $request->validate([
        'fname'   =>  'required|string|max:255',
        'lname'   =>  'required|string|max:255',
        'house'   =>  'required|string|max:255',
        'streets' =>  'required|string|max:255',
        'landmark'=>  'required|string|max:255',
        'state'   =>  'required',
        'district'=>  'required',
        'pin'     =>  'required|string|max:6|min:6',
        'gender'  =>  'required',
        'zone'    =>  'required',
        'unit'    =>  'required',
        'phone'   =>  'required|string|max:10|min:10',
        'email'   =>  'required|string|email|max:255|unique:users',
        'passwd'  =>  'required|string|min:6',

      ]);
      $user = new user;
      $user->fname    = $request->fname;
      $user->lname    = $request->lname;
      $user->house    = $request->house;
      $user->street   = $request->streets;
      $user->landmark = $request->landmark;
      $user->state    = $request->state;
      $user->district = $request->district;
      $user->pincode  = $request->pin;
      $user->gender   = $request->gender;
      $user->zone     = $request->zone;
      $user->unit     = $request->unit;
      $user->number   = $request->phone;
      $user->email    = $request->email;
      $user->password = bcrypt($request->passwd);
      $user->type     = "unit";
      $user->status   = 4;
      $user->save();
      Session::flash("msg","unit coordinator registration compleated");
      $user->Userid   = "SKTU".(1000+$user->id);
      $user->save();
      // $id=$user->id;
      // $user->userid = $id;
      // $user->save();
      return back();
    }
    public function zone(Request $request)
    {
      $request->validate([
        'fname'   =>  'required|string|max:255',
        'lname'   =>  'required|string|max:255',
        'house'   =>  'required|string|max:255',
        'streets' =>  'required|string|max:255',
        'landmark'=>  'required|string|max:255',
        'pin'     =>  'required|string|max:6|min:6',
        'state'   =>  'required',
        'district'=>  'required',
        'gender'  =>  'required',
        'zone'    =>  'required',
        'phone'   =>  'required|string|max:10|min:10',
        'email'   =>  'required|string|email|max:255|unique:users',
        'passwd'  =>  'required|string|min:6',

      ]);
      $user = new user;
      $user->fname    = $request->fname;
      $user->lname    = $request->lname;
      $user->house    = $request->house;
      $user->street   = $request->streets;
      $user->landmark = $request->landmark;
      $user->state    = $request->state;
      $user->district = $request->district;
      $user->pincode  = $request->pin;
      $user->gender   = $request->gender;
      $user->zone     = $request->zone;
      $user->number   = $request->phone;
      $user->email    = $request->email;
      $user->password = bcrypt($request->passwd);
      $user->type     = "zone";
      $user->status   = 4;
      $user->save();
      Session::flash("msg","Zone coordinator registration compleatly");
      $user->Userid   = "SKTZ".(1000+$user->id);
      $user->save();
      // $id=$user->id;
      // $user->userid = $id;
      // $user->save();
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->fname    = $request->fname;
        $user->lname    = $request->lname;
        $user->house    = $request->house;
        $user->street   = $request->streets;
        $user->landmark = $request->landmark;
        // $user->state    = $request->state;
        // $user->district = $request->district;
        $user->pincode  = $request->pin;
        // $user->gender   = $request->gender;
        // $user->zone     = $request->zone;
        // $user->unit     = $request->unit;
        $user->save();
        Session::flash("msg","your registration successfully Updates");
        return back();

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

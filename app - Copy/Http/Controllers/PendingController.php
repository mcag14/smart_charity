<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Session;

class PendingController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return view("chiefcordinator.accound_approvel");
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show()
  {
      //query to Database
     // $registers = DB::table('users')
     //              ->whereNull('status')
     //              ->get();
     $registers = User::where('status','3')->get();
      return view('chiefcordinator.accound_approvel' , ['registers'=>$registers]);
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update($id)
  {
      //user status Update
      $registers=user::find($id);
      $registers->status=4;
      $registers->userid   = "SKTM".(1000+$id);
      $registers->save();
      // Session::flash("msg","Registration Approved!");
      return back()->with('success','Registration Approved!');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
      //move from database when reject
      user::where('id','=',$id)->delete();
      return back();
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class guest extends Model
{
  // use Notifiable;
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'guest_id','name','email','type',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  // protected $hidden = [
  //     'password', 'remember_token',
  // ];
}

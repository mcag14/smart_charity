<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class guestpay extends Model
{
  use Notifiable;
  protected $fillable = [
    'amount','method','method_number','status',
  ];
}

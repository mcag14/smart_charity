<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('payments', function (Blueprint $table) {
          $table->increments('id');
          $table->date('date');
          $table->integer('user_id');
          $table->integer('amount');
          $table->string('method');
          $table->integer('paymentnumber')->unique();
          $table->integer('status');
          $table->rememberToken();
          $table->timestamps();
      });
      // Schema::create('monthly_pay', function (Blueprint $table) {
      //     $table->increments('id');
      //     $table->string('user_userid');
      //     $table->integer('amount');
      //     $table->string('method');
      //
      //     $table->rememberToken();
      //     $table->timestamps();
      // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

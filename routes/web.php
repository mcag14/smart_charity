<?php
use Illuminate\Support\Facades\Input as input;
use App\User;
Use App\Payment;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', function () {
    return view('test');
});
Route::get('/', function () {
    return view('index');
});

Auth::routes();
Route::get('/memberuser.changepassword',function()
{
  return view('memberuser.changepassword');
});

Route::get('status', 'PaymentController@show');
Route::post('payment','PaymentController@single');
Route::get('guestpayment',function()
{
  return view('guestpayment');
});
Route::get('guestpay','GuestController@guestpay');
// Route::get('guestpay',function()
// {
//   return view('chiefcordinator.guestpays');
// });
Route::post('/guestpayment','GuestController@guestpaym')->name('guestpaym');

Route::get('/memberuser.singlepay',function()
{
  return view('memberuser.singlepay');
});
Route::get('memberuser.status',function()
{
  return view('memberuser.status');
});

Route::get('/memberuser.monthlypay',function()
{
  return view('memberuser.monthlypay');
});

Route::get('/member',function()
{
  return view('memberuser.member');
});
Route::get('/reset',function()
{
  return view('reset');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/form',function (){
  return view('form');
});
Route::get('/guest',function()
{
  return view('guest');
});
Route::post('guestupdate','GuestController@guestupdate');
Route::get('/form','RegistrationController@index');
Route::post('/store','RegistrationController@store');
Route::post('/updatepassword','HomeController@updatepassword');
// Route::get('/guest','GuestController@index');
// Route::post('/store','GuestController@store');
Route::get('/donate',function()
{
  return view('donate');
});
Route::get('/login',function()
{
  return view('login');
});
Route::post('/logincustom','LoginController@Login')->name('logincustom');
Route::post('/singlepay','PaymentController@single')->name('singlepay');
// Route::get('/chiefcordinator.chief',function()
// {
//   return view('chiefcordinator.chief');
// })->name('chiefcordinator.chiefHome');

Route::get('status',function()
{
  return view('memberuser.status');
})->name('memberuser.userHome');

Route::get('/zonecordinator.zoneadmin',function()
{
  return view('zonecordinator.zoneadmin');
})->name('zonecordinator.zoneHome');

// Route::get('/unitcordinator.unitadmin',function()
// {
//   return view('unitcordinator.unitadmin');
// })->name('unitcordinator.unitHome');

Route::get('/chiefdashboard', 'HomeController@chiefdashboard')->name('chiefcordinator.chiefHome');

// Route::get('/dashboard',function()
// {
//   return view('chiefcordinator.chief');
// });
Route::get('/pending',function()
{
  return view('chiefcordinator.accound_approvel');
});
Route::get('/dashboard', 'PaymentController@dashboard');
// Route::get('/dashboard',function()
// {
//   return view('chiefcordinator.Chief');
// });
Route::get('/payments',function()
{
  return view('chiefcordinator.payments');
});

Route::get('pending', 'PendingController@show');  //for display registration form
Route::get('/update/{id}','PendingController@update');
Route::get('/destroy/{id}','PendingController@destroy');
Route::get('/registration',function()
{
  return view('chiefcordinator.registration');
});
Route::post('/unit','RegistrationController@unit');
Route::post('/zone','RegistrationController@zone');
Route::get('manage', 'HomeController@manage');
Route::post('show_guest_payment','GuestController@showguestpayment');
Route::get('/payupdate/{id}','GuestController@payupdate');
Route::get('/paydestroy/{id}','GuestController@paydestroy');
Route::get('memberpays','PaymentController@memberpays');
Route::get('/memberpayupdate/{id}','PaymentController@memberpayupdate');
Route::get('/memberpaydestroy/{id}','PaymentController@memberpaydestroy');
Route::get('chiefsummery',function()
{
  return view('chiefcordinator.summary');
});
Route::post('summery', 'TestController@summery');
Route::post('test', 'TestController@test');
Route::get('/unitdashboard', 'HomeController@unitdashboard')->name('unitcordinator.unitHome');
Route::get('/unitprofile','TestController@test1');
Route::post('edit','RegistrationController@edit');
Route::get('unitpending','TestController@unitpending');
Route::get('unitpayment','TestController@test2');
Route::get('unitsummery','TestController@test2');

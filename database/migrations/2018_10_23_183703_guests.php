<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Guests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('guests', function (Blueprint $table) {
          $table->increments('id');
          $table->string('guest_id')->nullable();
          $table->string('name');
          $table->string('email');
          $table->string('type');
          $table->timestamps();
      });
      Schema::create('guestpays', function (Blueprint $table) {
          $table->increments('id');
          // $table->string('guest_id');
          $table->date('date');
          $table->integer('amount');
          $table->string('method');
          $table->integer('method_number')->unique();
          $table->integer('status');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guests');
        Schema::dropIfExists('guestpays');
    }
}

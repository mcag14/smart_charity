<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('userid')->nullable();
            $table->string('fname');
            $table->string('lname');
            $table->string('house');
            $table->string('street');
            $table->string('landmark');
            $table->string('state');
            $table->string('district');
            $table->string('pincode');
            $table->string('gender');
            $table->string('zone')->nullable();
            $table->string('unit')->nullable();
            $table->string('number');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('type');
            $table->integer('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([

      'userid'=>'SKTM1001',
      'fname'=>'Thwalhath',
      'lname'=>'PP',
      'house'=>'Puthan Peedikayil',
      'street'=>'Mudavantheri',
      'landmark'=>'Nadapuram',
      'state'=>'Kerala',
      'district'=>'Kozhikkode',
      'pincode'=>'673505',
      'gender'=>'male',
      'number'=>'9539011586',
      'email'=>'thwalhathpp786@gmail.com',
      'password'=>'123456'
      'type'=>'chief',
      'status'=>'4'
     ]);
    }
}

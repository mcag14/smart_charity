<?php

namespace App\Http\Controllers;
use App\user;
use Session;
use Auth;
use Carbon\Carbon;
use App\Payment;
use App\guestpay;
use Illuminate\Http\Request;
use Illuminate\support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function updatepassword(Request $request)
    {

      $this->validate($request, [
        'password' => 'min:6',
        'cpsw' => 'required_with:password|same:password|min:6'

      ]);

      $user   = Auth::user();
      // dump($user);
      // return;
      $user->password = bcrypt($request->passwd);
      $user->save();
      Session::flash("msg","password Successfully Updated");
      return back();
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function manage()
    {
      $units = User::where('status','3')->get();

       $data =  array();
      $data['units']      =  User::where('type','unit')->get();
      $data['zones']      =  User::where('type','zone')->get();
      return view('chiefcordinator.manage',compact("data"));
    }
    public function unitdashboard()
    {
      return "a";
      $mytime = Carbon::now();
      $data = array();
      $data['pending']= User::where('unit',Auth::user()->unit)->where('type','member')->where('Status','1')->get();
      $data['totalmember'] = User::where('type','member')->count();
      $data['unitmem'] = User::where('unit',Auth::user()->unit)->where('type','member')->pluck('id');
      $data['total'] = Payment::whereIn('user_id',$data['unitmem'])->sum('amount');
      $data['today'] = Payment::where('date',date("Y-m-d"))->whereIn('user_id',$data['unitmem'])->sum('amount');
      $data['paypending'] = Payment::whereIn('user_id',$data['unitmem'])->where('status','1')->get();
      return view('unitcordinator.unitadmin',compact("data"));
    }
    public function chiefdashboard()
    {
      $data = array();
      $data['total'] = Payment::all();
      $data['guest'] = guestpay::all();
      $data['today'] = Payment::where('date',date("Y-m-d"))->sum('amount') + guestpay::where('date',date("Y-m-d"))->sum('amount');
      $data['totalmember'] = User::where('type','member')->count();
      $data['pending']= User::where('Status','3')->get();
      $data['paypending'] = Payment::where('status','3')->get();
      $data['guestpay'] = guestpay::where('status',1);
      $data['zone1']= Payment::whereIn('user_id',User::where('zone','Kozhikode')->where('type','member')->pluck('id'))->get();
      $data['zone2']= Payment::whereIn('user_id',User::where('zone','Malappuram')->where('type','member')->pluck('id'))->get();
      $data['zone3']= Payment::whereIn('user_id',User::where('zone','Ernakulam')->where('type','member')->pluck('id'))->get();
      $data['zone4']= Payment::whereIn('user_id',User::where('zone','Thiruvananthapuram')->where('type','member')->pluck('id'))->get();
      $data['unit11']= Payment::whereIn('user_id',User::where('unit','Kozhikode')->where('type','member')->pluck('id'))->get();
      $data['unit12']= Payment::whereIn('user_id',User::where('unit','kannur')->where('type','member')->pluck('id'))->get();
      $data['unit13']= Payment::whereIn('user_id',User::where('unit','Wayanad')->where('type','member')->pluck('id'))->get();
      $data['unit14']= Payment::whereIn('user_id',User::where('unit','Kasargod')->where('type','member')->pluck('id'))->get();
      $data['unit21']= Payment::whereIn('user_id',User::where('unit','Malappuram')->where('type','member')->pluck('id'))->get();
      $data['unit22']= Payment::whereIn('user_id',User::where('unit','Palakkad')->where('type','member')->pluck('id'))->get();
      $data['unit23']= Payment::whereIn('user_id',User::where('unit','Thrissur')->where('type','member')->pluck('id'))->get();
      $data['unit31']= Payment::whereIn('user_id',User::where('unit','kochi')->where('type','member')->pluck('id'))->get();
      $data['unit32']= Payment::whereIn('user_id',User::where('unit','Alappuzha')->where('type','member')->pluck('id'))->get();
      $data['unit33']= Payment::whereIn('user_id',User::where('unit','Idukki')->where('type','member')->pluck('id'))->get();
      $data['unit41']= Payment::whereIn('user_id',User::where('unit','Thiruvananthapuram')->where('type','member')->pluck('id'))->get();
      $data['unit42']= Payment::whereIn('user_id',User::where('unit','Kollam')->where('type','member')->pluck('id'))->get();
      $data['unit43']= Payment::whereIn('user_id',User::where('unit','Kottayam')->where('type','member')->pluck('id'))->get();
      $data['unit44']= Payment::whereIn('user_id',User::where('unit','Pathanamthitta')->where('type','member')->pluck('id'))->get();
      return view('chiefcordinator.chief',compact("data"));
    }

}

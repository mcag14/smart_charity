<?php

namespace App\Http\Controllers;
use App\User;
Use Auth;
use Session;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected function redirectTo($view)
    {
      return redirect()->route($view);
    }
    public function _construct()
    {
      $this->middleware('guest')->except('logout');
    }
    public function Login(Request $request)
    {
      $request->validate([
        'email' => 'required|email',
        'psw'   => 'required',
      ]);
      $user =new User;
      $loginuser = $user->where('email','=',$request->email)->first();
      if(isset($loginuser->id))
      {
        switch ($loginuser->type) {
          case 'chief':
            $view = 'chiefcordinator.chiefHome';
            break;
          case 'unit':
            $view = 'unitcordinator.unitHome';
            break;
          case 'zone':
            $view = 'zonecordinator.zoneHome';
            break;
          case 'member':
            $view = 'memberuser.userHome';
            break;
          default:
            $view = '/';
            break;
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->psw])) {
          return redirect()->route($view);
        }
        else {
          Session::flash("invalid data");
          return back();
        }
      }
      else {
        Session::flash("invlaid dakfjlkdsja");
        return back();
      }
    }
}

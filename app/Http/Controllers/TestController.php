<?php

namespace App\Http\Controllers;
use App\guestpay;
use App\Payment;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{
  public function summery(Request $request)
  {
    $from = $request->from;
    $to  =  $request->to;
    $data = array();
    $data['member'] = Payment::whereBetween('date', [$from,$to])->get();
    $data['guest'] = guestpay::whereBetween('date', [$from,$to])->get();
    $data['zone1']=User::where('zone','Kozhikode')->where('type','member')->pluck('id');
    $data['zone2']=User::where('zone','Malappuram')->where('type','member')->pluck('id');
    $data['zone3']=User::where('zone','Ernakulam')->where('type','member')->pluck('id');
    $data['zone4']=User::where('zone','Thiruvananthapuram')->where('type','member')->pluck('id');
    $data['unit11']=User::where('unit','Kozhikode')->where('type','member')->pluck('id');
    $data['unit12']=User::where('unit','Wayanad')->where('type','member')->pluck('id');
    $data['unit13']=User::where('unit','Kannur')->where('type','member')->pluck('id');
    $data['unit14']=User::where('unit','Kasargod')->where('type','member')->pluck('id');
    $data['unit21']=User::where('unit','Malappuram')->where('type','member')->pluck('id');
    $data['unit22']=User::where('unit','Palakkad')->where('type','member')->pluck('id');
    $data['unit23']=User::where('unit','Thrissur')->where('type','member')->pluck('id');
    $data['unit31']=User::where('unit','Kochi')->where('type','member')->pluck('id');
    $data['unit32']=User::where('unit','Alappuzha')->where('type','member')->pluck('id');
    $data['unit33']=User::where('unit','Idukki')->where('type','member')->pluck('id');
    $data['unit41']=User::where('unit','Thiruvananthapuram')->where('type','member')->pluck('id');
    $data['unit42']=User::where('unit','Kollam')->where('type','member')->pluck('id');
    $data['unit43']=User::where('unit','Kottayam')->where('type','member')->pluck('id');
    $data['unit44']=User::where('unit','Pathanamthitta')->where('type','member')->pluck('id');
    return view('chiefcordinator.summary',compact("data"));
  }
  public function test(Request $request)
  {
    return view('chiefcordinator.summary');
  }
  public function test1()
  {
    $data = array();
    $data['pending']= User::where('unit',Auth::user()->unit)->where('type','member')->where('Status','1')->get();
    $data['unitcordinator'] = User::find(Auth::user()->id);
    return view('unitcordinator.unitprofile',compact("data"));
  }
  public function unitpending()
  {
    $data = array();
    $data['registers'] = User::where('status','1')->where('unit',Auth::user()->unit)->get();
    $data['pending']= User::where('unit',Auth::user()->unit)->where('type','member')->where('Status','1')->get();
     return view('unitcordinator.unitpending' , compact("data"));
  }
 public function test2()
 {
   $data = array();
   $data['unitmem'] = User::where('unit',Auth::user()->unit)->where('type','member')->pluck('id');
   $data['pay'] = Payment::whereIn('user_id',$data['unitmem'])->where('status','1')->get();
   $data['payment'] = User::where('status','1')->where('unit',Auth::user()->unit)->get();
   $data['pending']= User::where('unit',Auth::user()->unit)->where('type','member')->where('Status','1')->get();
    return view('unitcordinator.unit_payment_pending' , compact("data"));
 }
}
